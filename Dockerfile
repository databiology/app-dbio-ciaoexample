#
#   CIAO Mondo Application Dockerfile
#

# app/dbio/base:4.2.2 is based on Ubuntu 18.04
FROM app/dbio/base:4.2.2

# Entrypoint is defined as /usr/local/bin/main.sh in app/dbio/base:4.2.2
COPY main.sh  /usr/local/bin/main.sh
RUN  chmod +x /usr/local/bin/main.sh
